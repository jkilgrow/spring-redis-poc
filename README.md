# Spring Redis POC
This is a little demo I whipped up to test and demonstrate how to use Spring Redis with Jedis on a remote cluster.

There is still a little to do here but it should basically work.

It is set up with an integration test that connects to a remote Redis cluster at 10.91.44.102.  
If the integration test fails, it is probably because redis is not at that ip address.  
Reconfigure it for an available redis cluster.

