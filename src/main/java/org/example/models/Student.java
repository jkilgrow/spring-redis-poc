package org.example.models;

import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("Student")
public class Student implements Serializable
{
    public enum Gender
    {
        MALE, FEMALE
    }

    private String id;
    private String firstName;
    private String lastName;
    private Gender gender;
    private int grade;
}
