package org.example.tests;

import org.example.config.RedisPOCConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import redis.clients.jedis.Jedis;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RedisPOCConfiguration.class)
public class JedisIntegrationTest
{
    @Autowired
    private Jedis jedis;

    @Test
    public void testJedis()
    {
        jedis.set("myAwesomeKey", "myAwesomeValue");
        String response = jedis.get("myAwesomeKey");

        assertEquals("The values should be equivalent", "myAwesomeValue", response);
    }
}
