package org.example.tests;

import org.example.config.RedisPOCConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RedisPOCConfiguration.class)
public class JedisConnectionFactoryTest
{
    @Autowired
    private JedisConnectionFactory jedisConnectionFactory;

    @Test
    public void testJedisConnectionFactory()
    {

    }
}
